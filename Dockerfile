FROM maven:3.8.4-jdk-8
COPY target/springboot-demo-ms-0.0.1-SNAPSHOT.jar /usr/src/app/springboot-demo-ms-0.0.1-SNAPSHOT.jar
WORKDIR /usr/src/app
EXPOSE 8181
CMD ["java", "-jar", "./springboot-demo-ms-0.0.1-SNAPSHOT.jar"]