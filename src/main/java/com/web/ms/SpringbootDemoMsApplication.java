package com.web.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringbootDemoMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDemoMsApplication.class, args);
	}

	@GetMapping(value = "/")
	public ResponseEntity<String> index(){
		return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
	}

}
